import java.io.PrintStream;
import java.util.*;

public class BstMultiset<T> extends Multiset<T>
{
	public BstMultiset() {
		mRoot = null;
	} // end of BstMultiset()

	public void add(T item) {
		Node newNode = new Node(item);
		
		Node currNode = mRoot;
		Node parentNode = null;
		
		// Root is empty, insert our first node.
		if (mRoot == null) {
			mRoot = newNode;
			return;
		}
		
		// Begin to traverse the tree.
		while(true) {
			parentNode = currNode;
			// Traverse the left subtree.
			if (((String) item).compareTo((String) currNode.mKey) < 0) {
				currNode = currNode.mLeft;
				if (currNode == null) {
					parentNode.mLeft = newNode;
					return;
				}
			}
			// Traverse the right subtree.
			else if (((String) item).compareTo((String) currNode.mKey) > 0) {
				currNode = currNode.mRight;
				if (currNode == null) {
					parentNode.mRight = newNode;
					return;
				}
			}
			// It's a match, update occurence count.
			else {
				currNode.mCount++;
				return;
			}
		}
		
	} // end of add()
	

	public int search(T item) {
		Node currNode = mRoot;
				
		// Traverse the tree.
		while (currNode != null) {
			// Add to counter if we get a hit.
			if (currNode.mKey.equals(item)) {
				return currNode.mCount;
			}
			// Continue to traverse regardless.
			// Go left.
			if (((String) currNode.mKey).compareTo((String) item) > 0) {
				currNode = currNode.mLeft;
			}
			// Go right.
			else {
				currNode = currNode.mRight;
			}
		}
		
		return 0;
	} // end of search()


	public void removeOne(T item) {	
		// Is it even worth progressing?
		if (mRoot == null) {
			return;
		}
		
		Node parentNode = mRoot;
		Node currNode = mRoot;
		
		// Find node, and establish parental status.
		while (!currNode.mKey.equals(item)) {
			// Always keep track of the parent node as we traverse.
			parentNode = currNode;
			// Go left.
			if (((String) currNode.mKey).compareTo((String) item) > 0) {
				currNode = currNode.mLeft;
			}
			// Go right.
			else {
				currNode = currNode.mRight;
			}
			// We didn't find anything.
			if (currNode == null) {
				return;
			}
		}
		
		// There's more than one instance, no need to delete.
		if (currNode.mCount > 1) {
			currNode.mCount--;
			return;
		}
		
		// Three cases
		// Node to be deleted is a leaf node ( without children )
		// Node to be deleted has only one child
		// Node to be deleted has two children.
		
		// Node has no children.
		// Parent to wipe clean both branches, disconnecting our target node.
		if ((currNode.mLeft == null) && (currNode.mRight == null)) {
			if (currNode == mRoot) {
				mRoot.mKey = null;
				}
			else if (parentNode.mLeft != null) {
				parentNode.mLeft = null;
			}
			else {
				parentNode.mRight = null;
			}
		}
		// Node has one child.
		// Transfer child to same branch of parent that deleted node occupied.
		// Child is on the left.
		if (currNode.mRight == null) { 		
			// Target is root, it has no right child, so promote left child to root position.
			if (currNode == mRoot) {
				mRoot = currNode.mLeft;
			}
			else if (parentNode.mLeft != null) {
				parentNode.mLeft = currNode.mLeft;
			}
			else {
				parentNode.mRight = currNode.mLeft;
			}
		}
		// Child is on the right.
		else if(currNode.mLeft == null) {
			// Target is root, it has no left child, so promote right child to root position.
			if (currNode == mRoot) {
				mRoot = currNode.mRight;
			}
			else if (parentNode.mLeft != null) {
				parentNode.mLeft = currNode.mRight;
			}
			else {
				parentNode.mRight = currNode.mRight;
			}
		}
		// Node has two children.
		// We can replace the target with either of the children.
		else if ((currNode.mLeft != null) && (currNode.mRight != null)) {
			// We'll go find the smallest node in the right sub-tree.
			Node successor = getSuccessor(currNode);
			// Replace root with the successor.
			if (currNode == mRoot) {
				mRoot = successor;
			}
			else if (parentNode.mLeft != null) {
				parentNode.mLeft = successor;
			}
			else {
				parentNode.mRight = successor;
			}
			
			successor.mLeft = currNode.mLeft;
		}
		
		return;
		
	} // end of removeOne()
	
	public Node getSuccessor(Node deleteNode) {		
		// Let's discover the smallest element in this sub-tree.
		Node successor = null;
		Node sucessorParent = null;
		Node currNode = deleteNode.mRight;
		
		while (currNode != null) {
			sucessorParent = successor;
			successor = currNode;
			currNode = currNode.mLeft;
		}
		if (successor != deleteNode.mRight) {
			sucessorParent.mLeft = successor.mRight;
			successor.mRight = deleteNode.mRight;
		}
		
		return successor;
	}
	
	
	public void removeAll(T item) {		
		// We'll work out how many removal operation are required.
		int count;
		count = search(item);
		
		// Let's rely on our existing method to do the heavy lifting. 
		for (int x = 0; x < count; x++) {
			removeOne(item);
		}
		
	} // end of removeAll()


	public void print(PrintStream out) {	
		Node currNode = mRoot;
		
		// There's nothing to print.
		if (currNode == null) {
			return;
		}
		
		// Print using a recursion.
		printTree(currNode);
		System.out.println("");
		
	} // end of print()
	
	private void printTree(BstMultiset<T>.Node currNode) {
		// Let's go left.
		if (currNode.mLeft != null) {
			printTree(currNode.mLeft);
		}
		// Print.
		System.out.print(currNode.mKey + " | " + currNode.mCount + "\n");
		// Let's go right.
		if (currNode.mRight != null) {
			printTree(currNode.mRight);
		}
	}


	private Node mRoot;
	
	private class Node {	
		private T mKey;				// associated data
		private Node mLeft, mRight;	// left and right subtrees
		private int mCount;			// amount of occurrences
		
		public Node(T item) {
			mKey = item;
			mLeft = null;
			mRight = null;
			mCount = 1;
		}
		
	}

} // end of class BstMultiset
