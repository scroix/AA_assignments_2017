import java.io.PrintStream;

public class LinkedListMultiset<T> extends Multiset<T>
{

	/* Reference to head of list. */
	protected Node mHead;
	/* Reference to tail of list. */
	protected Node mTail;
	/* Length of list. */
	protected int mLength;

	
	public LinkedListMultiset() {
		mHead = null;
		mTail = null;
		mLength = 0;
	} // end of LinkedListMultiset()


	public void add(T item) {
		Node newNode = new Node(item), currNode = mHead;

		// If head is empty, then list is empty and head and tail references need to be initialised.
		if (mHead == null) {
			mHead = newNode;
			mTail = newNode;
			mLength++;
			return;
		}

		// Single element in list thats identical
		if( newNode.equals(mHead) && ((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ){
			currNode.addCount();
			mLength++;
			return;
		}

		// More than one element in list thats identical
		while(currNode.getNext() != null){
			if(((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ) {
				currNode.addCount();
				mLength++;
				return;
			}
			currNode = currNode.getNext();
		}

		// More than one identical element where same value is at tail
		if(currNode == mTail && ((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ){
			currNode.addCount();
			mLength++;
			return;
		}
		
		newNode.setNext(mHead);
		mHead.setPrev(newNode);
		mHead=newNode;
		mLength++;

	} // end of add()

	public int search(T item) {
		Node currNode = mHead;
		
		int count = 0;
		
		if(currNode == null)
			return count;
		
		// find in head or tail
		if(mHead.getValue().equals(item))
			return mHead.getCount();

		if(mTail.getValue().equals(item))
			return mTail.getCount();

		// Find the item in the rest of the list
		while(currNode.getNext()!=null){
			if(currNode.getValue().equals(item))
				count = currNode.getCount();
			currNode = currNode.getNext();
		}
		return count;
	} // end of search()

	public void removeOne(T item) {
		if(mHead == null)
			return;
		
		Node currNode = mHead;
		// value is at head node
		if (currNode.getValue().equals(item)) {
			// check if length of 1
			if (mLength == 1) {
				mHead = mTail = null;
			} else if(currNode.getCount() > 1) {
				currNode.minCount();
			} else { //List is greater than 1
				currNode.setPrev(null);
				mHead = currNode.getNext();
//				mHead.setPrev(null);
				currNode = null;
			}
			mLength--;
		}
		// search for value in the rest of list
		else {
			currNode = currNode.getNext();

			while (currNode != null) {
				if (currNode.getValue().equals(item)) {
					if(currNode.getCount() > 1){
						currNode.minCount();
						mLength--;
						return;
					}
					Node prevNode = currNode.getPrev();
					prevNode.setNext(currNode.getNext());
					// check if tail
					if (currNode.getNext() != null) {
						currNode.getNext().setPrev(prevNode);
					}
					else {
						mTail = prevNode;
					}
					currNode = null;
					mLength--;
					return;
				}

				currNode = currNode.getNext();
			}

		}

	} // end of removeOne()

	public void removeAll(T item) {
		Node currNode = mHead;
		Node prevNode = null;
		
		if(currNode == null)
			return;

		// value is at head node or multiple occurrences
		while (currNode != null && currNode.getValue().equals(item)) {
			mHead = currNode.getNext();
			currNode = mHead;
			mLength--;
		}

		// remove other occurrences
		while (currNode != null) {
			if (currNode.getValue().equals(item)) {
				while(currNode.getCount() > 1){
					currNode.minCount();
				}
				prevNode.setNext(currNode.getNext());
				
				// check if tail
				if (currNode.getNext() != null) {
					currNode.getNext().setPrev(prevNode);
				}
				else {
					mTail = prevNode;
				}
				
				currNode = null;
				mLength--;
				return;
			}
			prevNode = currNode;
			currNode = currNode.getNext();
		}

	} // end of removeAll()


	public void print(PrintStream out) {
		if(mHead == null) {
			return;
		}
		Node currNode = mHead;

		while (currNode != null) {
			System.out.print(currNode.getValue() + " | " + currNode.getCount() + "\n");
			// next node
			currNode = currNode.getNext();
		}

	} // end of print()

	class Node {
		/* Stored value of node. */
		private T mValue;
		/* Reference to next node. */
		private Node mNext;
		/* Reference to previous node. */
		private Node mPrev;
		/* Private count for the node */
		private int mCount;

		public Node(T item) {
			this.mValue = item;
			this.mNext = null;
			this.mPrev = null;
			this.mCount = 1;
		}

		public T getValue() {
			return mValue;
		}

		public Node getNext() {
			return mNext;
		}

		public Node getPrev() {
			return mPrev;
		}

		public void setValue(T value) {
			mValue = value;
		}

		public void setNext(Node next) {
			this.mNext = next;
		}

		public void setPrev(Node prev) {
			this.mPrev = prev;
		}

		public int getCount() {
			return mCount;
		}

		public void setCount(int mCount) {
			this.mCount = mCount;
		}

		public void addCount() {
			this.mCount++;
		}

		public void minCount() {
			this.mCount--;
		}

	} // end of inner class Node

} // end of class LinkedListMultiset