import java.io.PrintStream;

public class SortedLinkedListMultiset<T> extends Multiset<T>
{
	/* Reference to head of list. */
	protected Node mHead;
	/* Reference to tail of list. */
	protected Node mTail;
	/* Length of list. */
	protected int mLength;

	public SortedLinkedListMultiset() {
		mHead = null;
		mTail = null;
		mLength = 0;
	} // end of SortedLinkedListMultiset()

	@Override
	public void add(T item) {
		// Set up an iterator and the node to insert
		Node newNode = new Node(item), currNode = mHead, prevNode = null;

		// Empty list scenario
		if(mHead == null) {
			mHead = newNode;
			mTail = newNode;
			mLength++;
			return;
		}

		// Single element in list thats identical
		if( mLength == 1 && ((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ){
			currNode.addCount();
			mLength++;
			return;
		}

		// More than one element in list thats identical
		while(currNode.getNext() != null){
			if(((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ) {
				currNode.addCount();
				mLength++;
				return;
			}
			currNode = currNode.getNext();
		}

		// More than one identical element where same value is at tail
		if(currNode== mTail && ((String) newNode.getValue()).equalsIgnoreCase((String) currNode.getValue()) ){
			currNode.addCount();
			mLength++;
			return;
		}


		currNode = mHead;
		// New node is smaller than head, add it to start of list
		if(((String) newNode.getValue()).compareTo((String)mHead.getValue() ) < 0 ) {
			newNode.setNext(mHead);
			mHead.setPrev(newNode);
			mHead = newNode;
		}
		// Get to the proper spot
		else {
			while(currNode.getNext() != null ) {
                // Integer value describing the difference in our node and new data.
                // Keep iterating through while we arent at the correct spot
                int compare = ((String) newNode.getValue()).compareTo((String)currNode.getValue());
				if(compare > 0) {
					prevNode = currNode;
					currNode = currNode.getNext();
				} else {
					break;
				}
			}

			// Find where to insert node
			int compare = ((String) newNode.getValue()).compareTo((String)currNode.getValue());
			if(compare < 0) {
				prevNode.setNext(newNode);
				newNode.setPrev(prevNode);

				newNode.setNext(currNode);
				currNode.setPrev(newNode);
				mLength++;
				return;
			}

			// Add at tail, update tail
			if(currNode == mTail ) {
				// Here there may be one element, but new node is greater than tail.
				// Need to update tail to newNode

				newNode.setPrev(currNode);
				currNode.setNext(newNode);
				mTail = newNode;
				mLength++;
				return;
			}
		}
	} // end of add()


	public int search(T item) {
		Node currNode = mHead;
		int count = 0;

		if(currNode == null)
			return count;

		// find in head or tail
		if(mHead.getValue().equals(item))
			return mHead.getCount();

		if(mTail.getValue().equals(item))
			return mTail.getCount();

		// Find the item in the rest of the list
		while(currNode.getNext()!=null){
			if(currNode.getValue().equals(item))
				count = currNode.getCount();
			currNode = currNode.getNext();
		}
		return count;
	} // end of search()

	@Override
	public void removeOne(T item) {
		if(mHead == null)
			return;

		Node currNode = mHead;
		// value is at head node
		if (currNode.getValue().equals(item)) {
			// check if length of 1
			if (mLength == 1) {
				mHead = mTail = null;
			} else if(currNode.getCount() > 1) {
				currNode.minCount();
			} else { //List is greater than 1
				mHead = currNode.getNext();
				mHead.setPrev(null);
				currNode = null;
			}
			mLength--;
		}
		// search for value in the rest of list
		else {
			currNode = currNode.getNext();

			while (currNode != null) {
				if (currNode.getValue().equals(item)) {
					if(currNode.getCount() > 1){
						currNode.minCount();
						mLength--;
						return;
					}
					Node prevNode = currNode.getPrev();
					prevNode.setNext(currNode.getNext());
					// check if tail
					if (currNode.getNext() != null) {
						currNode.getNext().setPrev(prevNode);
					}
					else {
						mTail = prevNode;
					}
					currNode = null;
					mLength--;
					return;
				}

				currNode = currNode.getNext();
			}

		}

	} // end of removeOne()

	@Override
	public void removeAll(T item) {
		Node currNode = mHead;
		Node prevNode = null;

		if(currNode.equals(null))
			return;

		// value is at head node or multiple occurrences
		while (currNode != null && currNode.getValue().equals(item)) {
			mHead = currNode.getNext();
			currNode = mHead;
			mLength--;
		}

		// remove other occurrences
		while (currNode != null) {
			if (currNode.getValue().equals(item)) {
				while(currNode.getCount() > 1){
					currNode.minCount();
				}
				prevNode.setNext(currNode.getNext());

				// check if tail
				if (currNode.getNext() != null) {
					currNode.getNext().setPrev(prevNode);
				}
				else {
					mTail = prevNode;
				}

				currNode = null;
				mLength--;
				return;
			}
			prevNode = currNode;
			currNode = currNode.getNext();
		}

	} // end of removeAll()

	@Override
	public void print(PrintStream out) {
		if(mHead ==null) {
			return;
		}
		Node currNode = mHead;

		while (currNode != null) {
			System.out.print(currNode.getValue() + " | " + currNode.getCount() + "\n");
			// next node
			currNode = currNode.getNext();
		}
	} // end of print()

	private class Node {
		/* Stored value of node. */
		private T mValue;
		/* Reference to next node. */
		private Node mNext;
		/* Reference to previous node. */
		private Node mPrev;
		/* Count of node */
		private int mCount;

		public Node(T item) {
			mValue = item;
			mNext = null;
			mPrev = null;
			mCount = 1;
		}

		public T getValue() {
			return mValue;
		}

		public Node getNext() {
			return mNext;
		}

		public Node getPrev() {
			return mPrev;
		}

		public void addCount() {
			mCount++;
		}

		public void minCount() {
			mCount--;
		}

		public int getCount(){
			return mCount;
		}

		public void setNext(Node next) {
			mNext = next;
		}

		public void setPrev(Node prev) {
			mPrev = prev;
		}

	} // end of inner class Node
} // end of class SortedLinkedListMultiset
