import java.io.*;
import java.util.*;


public class Stopwatch
{
	// The words.
	List<String> words = new ArrayList<String>();
	
	// Our randomizer!
	private Random randomGenerator;
			
	// T addition and removals.
	String set_one[] = {"A"}; 								// 100% Additions
	String set_two[] = {"RO"};								// 100% Removals
	String set_three[] = {"A", "RO"};							// 50% Each

	// Searches.
	String set_four[] = {"A", "RO", "S"} ;					// 33% Each
	String set_five[] = {"A", "RO", "S", "S"}; 				// 50% Search
	String set_six[] = {"A", "RO", "S", "S", "S", "S", "S"}; 	// ~70% Search
	
	// All of them!
	String sets[][] = {set_one, set_two, set_three, set_four, set_five, set_six};
	String set_names[] = {"just additions", "just removals", "balanced a & ro", "equal search, a & ro", "half search", "mostly search"};
	
	public Stopwatch(String size) {
		// We'll read from this hard-coded file and add to our array.
		File file = new File("general.txt");
		
		// Populate 'words' from a file to the scale (e.g. 1000 words) passed by the user.
		populate(file, Integer.parseInt(size));
				
		// Shuffle
		Collections.shuffle(words);
		
		// Generate our randomizer..!
		randomGenerator = new Random();
	}
	
	public void populate(File file, int size) {
		// Our word limit.
		int count = 0;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			// Until either our dictionary is empty, or our word limit is reached.
			while ( ((line = reader.readLine()) != null) && (count < size) ) {
				words.add(line);
				count++;
			}
			reader.close();
		}
		catch (Exception e) {
		    System.err.format("Exception occurred trying to read '%s'.", file);
		    e.printStackTrace();
		}	
	}
	
	public void measure(Multiset<String> multiset, int iteration) {
		// Retrieve corresponding set.
		String set[] = sets[iteration];
		
		// We're going to make a duplicate multiset of the one passed to us.
		// This means we're using a fresh multiset each test.
		String class_name = multiset.getClass().getSimpleName();
		Multiset<String> duplicate = implementation(class_name);
		
		// We'll always start fresh by adding our elements, there's no need to time this!
		for(String word : words) {
			duplicate.add(word);
		}

		// Start the timer!
		long startTime = System.nanoTime();
		
		// Let's instantiate our variables for the random draw.
		int index;
        String operator;
		
		for(String word : words) {
			// For every word in our shuffled set, we'll pick an element from the 
			// operator array and perform the appropriate operation.
			index = randomGenerator.nextInt(set.length);
			operator = set[index];

			// The time cost will be constant amongst all tests conducted.
			switch (operator) {
				case "A":
					duplicate.add(operator + " " + word);
					break;
				case "RO":
					duplicate.removeOne(operator + " " + word);
					break;
				case "S":
					duplicate.search(operator + " " + word);
					break;
				default:
					System.out.println("Invalid operator generated from set.");
			}
			
		}

		// ...and stop!
		long endTime = System.nanoTime();
		
		String set_name = set_names[iteration];
		System.out.println(((double)(endTime - startTime)) / Math.pow(10, 9) + " sec for " + set_name);
			
	}
	
	public Multiset<String> implementation(String type) {
		// determine which implementation to test
		Multiset<String> multiset = null;
		switch(type) {
			case "LinkedListMultiset":
				multiset = new LinkedListMultiset<String>();
				break;
			case "SortedLinkedListMultiset":
				multiset = new SortedLinkedListMultiset<String>();
				break;
			case "BstMultiset":
				multiset = new BstMultiset<String>();
				break;
			case "HashMultiset":
				multiset = new HashMultiset<String>();
				break;
			case "BalTreeMultiset":
				multiset = new BalTreeMultiset<String>();
				break;
			default:
				System.err.println("Unknown implmementation type.");
		}
		return multiset;
	}
	
	public void print() {
		// Let's keep it simple, stupid. Mwah..! ;)
		for(String word : words) {
			System.out.println(word);
		}
	}
	
}
